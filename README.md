# Interview

Interview project

During the task solution, the first approach that came to my mind is thread safety queue and concurrent queue.
First of all, I checked the documentation. https://docs.microsoft.com/pl-pl/dotnet/api/system.collections.concurrent.concurrentqueue-1?view=netframework-4.7.2
Next I checked documentation about synchronization mechanizm, one at time action, I used semaphore https://docs.microsoft.com/en-us/dotnet/api/system.threading.semaphoreslim?redirectedfrom=MSDN&view=netframework-4.7.2

Finnally, I checked other the stackoverflow solutions:
https://stackoverflow.com/search?q=thread+safety+queue+C%23
https://stackoverflow.com/questions/43117502/thread-safety-in-concurrent-queue-c-sharp
https://www.codeproject.com/Articles/1112510/TPL-Producer-Consumer-Pattern-Thread-Safe-Queue-Co

## Short coding guidelines
1. lowecase parameters
2. camelcase for fields and classes
3. interfaces with I
4. private fields with _ (underscore) at the beginning
5. in doubt please ask!
6. comments -> https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/xmldoc/xml-documentation-comments
7. others -> https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/index

# Version control guidelines

## The flow
* Commits with corrections (eg. done in response to code review feedback) mustn't be preserved as standalone commits, but squashed with the entire submission to keep version history clean.
* Make sure to understand the implications of rewriting version history, keep local backup and **DO NOT** overwrite `development` nor `master`.

* Whenever merged branches lag behind the target branch (`development` or `master`), they need to be **rebased** on the target branch first. We only merge *towards* trunk branches, not the other way around:
    * ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) `feature/PATRONSZCZ19-123-short-desc` → `development` is good.
    * ![#f03c15](https://placehold.it/15/f03c15/000000?text=+) ~~`development` → `features/PATRONSZCZ19-123-short-desc`~~ is bad, rebase `features/PATRONSZCZ19-123-short-desc` on `development` if it lags behind it.

## Branch naming
Branch naming follows the `type/PATRONSZCZ19-123-short-desc` convention where `123` stands for the number of the task on JIRA while `type` defines the general character of the work. It can be:
* `feature` - for submissions that add or enhance app's functionality
* `bugfix` - self-explanatory
* `refactoring` - for submissions that alter code without an impact on its functionality (eg. code cleanup or replacing a third-party dependency with another)
* `docs` - for documenting (adding files or updating README.md).

Other may be added on an as-needed basis.

## Commit messages

The following format is suggested for commit messages:

```
PATRONSZCZ-123: Brief description of the nature of submitted changes

Any additional information regarding the commit, 
for example its relation to other tasks (if applicable),
or a brief summary of pending TODOs.
```
It is recommended to briefly explain the nature of changes esp. if they're non-obvious. Eg.:

* ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)
```
PATRONSZCZ-718: Added initial mobile app discount for discount category

OldService replaced with NewService as service didn't work correctly for some reason
```
Avoid commit messages that are: 
* ![#f03c15](https://placehold.it/15/f03c15/000000?text=+) overly general:
    ```
    PATRONSZCZ19-123: Fix to PR
    ```
* ![#f03c15](https://placehold.it/15/f03c15/000000?text=+) focused on "what" instead of "why" (the former question is answered by code itself, commit message should answers the latter):
    ```
    PATRONSZCZ19-123: Added delay(1000L) to BankTransferInteractor.fetchFee()
    ```
If there is no corresponding task on Jira (acceptable for quick fixes and small code submissions), the same rules are recommended, apart from:
* Ommit the _"PATRONSZCZ19-123:"_ prefix from the commit message (obviously)
* Use the briefest possible description of the task as the branch name, eg. `bugfix/startup_crash`.