using System;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var random = new Random();
            var tm = new ConcurrentQueueActionWorker();

            Parallel.ForEach(Enumerable.Range(0, 1000), async number =>
            {
                await Task.Delay(100 * number);
                tm.Enqueue(() => Console.WriteLine($"Print {number}"));
            });

            Task.Delay(4000).Wait();
        }
    }
}