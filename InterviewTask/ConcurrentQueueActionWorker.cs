using System;
using System.Collections.Concurrent;
using System.Threading;

namespace InterviewTask
{
    public class ConcurrentQueueActionWorker
    {
        private readonly ConcurrentQueue<Action> _actionQueue = new ConcurrentQueue<Action>();
        private readonly SemaphoreSlim _actionSemaphoreSlim = new SemaphoreSlim(1, 1);

        /// <summary>
        ///     Add action into the queue and run it.
        /// </summary>
        /// <param name="action"></param>
        public async void Enqueue(Action action)
        {
            _actionQueue.Enqueue(action);

            await _actionSemaphoreSlim.WaitAsync();

            Process();

            _actionSemaphoreSlim.Release();
        }

        private void Process()
        {
            Console.WriteLine("Process...");

            if (_actionQueue.TryDequeue(out var action) == false)
            {
                throw new InvalidProgramException("ActionQueue is empty!");
            }

            action();

            Console.WriteLine($"Action done. Left: {_actionQueue.Count}");
        }
    }
}